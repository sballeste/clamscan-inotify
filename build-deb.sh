#!/bin/bash
# Build the debian package
# @author seb <seb@fitou.lan>

# Binary checks
[ -x "$(command -v git)" ]                  || \
    { echo >&2 "Install git!" && exit 1; }

[ -x "$(command -v asciidoctor-pdf)" ]      || \
    { echo >&2 "Install asciidoctor-pdf!" && exit 1; }

[ -x "$(command -v debchange)" ]            || \
    { echo >&2 "Install debchange!" && exit 1; }

[ -x "$(command -v dpkg-buildpackage)" ]    || \
    { echo >&2 "Install dpkg-buildpackage!" && exit 1; }

# Input message for change log
[[ -n "$*" ]] && INPUT="$*" || INPUT="$(cat -)"
[[ -z "$INPUT" ]] && read -r INPUT

# Fetch values in changelog and control
ROOT_PATH="$(realpath "$(dirname "$(readlink -f "$0")")")";
PACKAGE=$(grep -F 'Package:' "$ROOT_PATH/debian/control" | cut -d ' ' -f2)

# Development, fix and release are handled with git-flow
# @see https://danielkummer.github.io/git-flow-cheatsheet/
# @see https://nvie.com/posts/a-successful-git-branching-model/
dpkg -l git-flow | grep -c '^ii' 1>/dev/null                               || {\
    echo "Building and publishing $PACKAGE requires git-flow."             && \
    echo                                                                   && \
    echo " @see https://danielkummer.github.io/git-flow-cheatsheet/"       && \
    echo " @see  https://nvie.com/posts/a-successful-git-branching-model/" && \
    echo                                                                   && \
    echo "Please install git-flow and initialize this repository"          && \
    echo "with git flow init, answering questions with these values :"     && \
    cat << EOTXT

[gitflow "branch"]
    master = master
    develop = next
[gitflow "prefix"]
    feature = feature/
    bugfix = bugfix/
    release = release/
    hotfix = hotfix/
    support =
    versiontag = tag/
[gitflow "path"]
    hooks = $ROOT_PATH/.git/hooks

EOTXT
    echo "install : sudo apt install git-flow"              && \
    echo "init the repository : git flow init"              && \
    echo "git flow not initialized, $0 aborted" && exit 1; }

# git flow not installed, abort
git flow version 2>/dev/null || \
    (echo "git flow is not found, $0 aborted" && exit 1)

# Get commit type and reason from currrent git branch
COMMIT_HEADER="upstream: "$(git branch | grep '^\*\ ' | sed -r 's/\*\ (.*)/\1/')
[[ -z "$COMMIT_HEADER" ]] && ( echo "Can't find a valid git commit header !" &&  exit 1 )

# Get commit type and reason from currrent git flow based on git flow branch separator
git branch|grep '^\*\ '|grep -F '/' && \
    COMMIT_HEADER="$(git branch | grep '^\*\ ' | sed -r 's/\*\ (.*)\/(.*)/\1: \2/')"
[[ -z "$COMMIT_HEADER" ]] && ( echo "Can't find a valid git flow commit header !" &&  exit 1 )

# What is commited using dch, update changelog
debchange   --package "$PACKAGE" --increment                \
            --changelog "$ROOT_PATH/debian/changelog"       \
            "$COMMIT_HEADER ($INPUT)"
debchange   --release ignored

# What is now the version ?
VERSION=$(head -1 "$ROOT_PATH/debian/changelog"|cut -d' ' -f2|sed 's/(//'|sed 's/)//')

# Build man page - READM.adoc is the source file
asciidoctor-pdf --backend manpage                           \
                --out-file "$ROOT_PATH/debian/${PACKAGE}.1" \
                --attribute revnumber="$VERSION"            \
                --attribute package="$PACKAGE"              \
                --attribute packager="$DEBFULLNAME"         \
                "$ROOT_PATH/doc-src/README.adoc"

# Build readme file for cgit
export MANWIDTH=84
man "$ROOT_PATH/debian/${PACKAGE}.1" > "$ROOT_PATH/README"

# Prepare debian package directory
[[ ! -d "${ROOT_PATH}/debian/packages" ]] && \
    mkdir -v "${ROOT_PATH}/debian/packages";
find "${ROOT_PATH}/debian/packages/" -type f -exec rm {} \;

# Build debian package
dpkg-buildpackage --no-sign --build=all &&                                  \
    mv -vf "${ROOT_PATH}/../${PACKAGE}_"* "${ROOT_PATH}/debian/packages" && \
    ./dput-dev.sh

# Stop there if it was a test
echo "$INPUT" | grep -Fc 'test:' 1>& /dev/null && \
    { echo "It was a test, nothing was committed to git" && exit 0; }

# Add package generation for networks without local repository / build chain
git add debian/packages/clamscan-inotify_*

# Commit and log
git commit --all --message "$COMMIT_HEADER ($INPUT)"

# Publish the work : push the feature/fix/hostfix branch to origin
# for sharing purposes, not used yet
# GIT_FLOW_PUBLISH=$(git branch | grep '^\*\ ' | sed -r 's/\*\ (.*)\/(.*)/git flow \1 publish \2/')
# echo "$GIT_FLOW_PUBLISH"
# $GIT_FLOW_PUBLISH
# unset GIT_FLOW_PUBLISH

# Finish the work
GIT_FLOW_FINISH=$(git branch | grep '^\*\ ' | \
    sed -r 's/\*\ (.*)\/(.*)/git flow \1 finish \2/')
echo "$GIT_FLOW_FINISH"
# @todo test this command before execute it
$GIT_FLOW_FINISH
unset GIT_FLOW_FINISH

# Push code of current branch to origin
git push origin
git push codeberg

# Push tags (if release) to origin
git branch | grep '^\*\ release' && git push origin --tags
git branch | grep '^\*\ release' && git push codeberg --tags

