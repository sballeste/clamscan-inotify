#!/bin/sh
# Usage: cat <some file> | $0 "filepath"
# Can speak to notification-server Cinnamon, Deepin, Enlightenment, GNOME,
# GNOME Flashback and KDE Plasma
# @see https://developer.gnome.org/notification-spec/
# @see https://wiki.archlinux.org/index.php/Desktop_notifications
# Distributed by clamscan-inotify

# Input RESULT of clamdscan-verify, filtered
# The string has a form FILEPATH: RESULT
RESULT="$(cat - | head -n 1 | tr -d '\000' | cut -c -512)"

# Get Input parameters
FILEPATH=stream
[ -n "$*" ] && FILEPATH="$1"

# Default icon
ICON="clamscan-pass.svg"

# Default notify-send urgency
URGENCY='low'

# Default Summary for OK and signatures FOUND : remove FILEPATH from RESULT
SUMMARY="$(echo "$RESULT" | sed 's/^.*:\ //')"

# BODY information
BODY="$FILEPATH"

# Journalize when $0 is called from the nautilus-script "clamdscan" ...
LOGGER_DRYRUN_PARAM=''

# ... but don't journalize twice if $0 is called from clamdscan-inotify.sh
# or clamdscan-verify.sh, because first already do.
[ ! -z "$PPID" ] && [ -e "/proc/$PPID/comm" ]                          && \
    [ $(grep -Fc 'clamdscan-' "/proc/$PPID/comm" 2>/dev/null ) -gt 0 ] && \
        LOGGER_DRYRUN_PARAM='--no-act'

# Adapt icon if a SIGNATURE is found, and log result
[ "$(echo "$RESULT" | grep -Fc "FOUND" )" -ge 1 ] && \
    ICON="clamscan-failed.svg"                    && \
    URGENCY='normal'                              && \
    logger "$LOGGER_DRYRUN_PARAM" --priority 3 \
        --tag "clamdscan-inotify" "$RESULT for $(whoami)"

# Adapt icon if an ERROR is found, and log result
# Add the full error, shortened of "ERROR:" to body
[ "$(echo "$RESULT" | grep -Fc "ERROR" )" -ge 1 ]              && \
    ICON="clamscan-error.svg"                                  && \
    URGENCY='critical'                                         && \
    BODY="$(echo "$RESULT" | sed 's/^ERROR:\ //') ($FILEPATH)" && \
    logger "$LOGGER_DRYRUN_PARAM" --priority 2 \
        --tag "clamdscan-inotify" "$RESULT"

# Don't notify if no display available
[ -z "$DISPLAY" ] && exit

# Notify with notify-send
[ -x "$(command -v notify-send)" ] && \
    notify-send --icon="/usr/share/icons/Adwaita/scalable/status/$ICON" \
                --urgency="$URGENCY" "$SUMMARY" "$BODY"

