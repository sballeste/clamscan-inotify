#!/bin/bash
# Send filepath as arguments to clamdscan
# Usage: $0 "<filepath>"
# Distributed by clamscan-inotify

# Check that clamdscan is available or exit critical OS file missing
[ -x "$(command -v clamdscan)" ] || exit 72

# Get Input parameters
[ -n "$*" ] && INPUT="$1"

# Checks that input is consistant
[ ${#INPUT} -eq 0 ] && exit 0

# Dont check directory (inotify-wait send already each file changes)
[ -d "$INPUT" ] && exit 0

# Pass filepath to clamdscan, add a limit of 5mn
timeout 5m \
clamdscan   --config-file=/etc/clamav/clamscan-inotify-server.conf \
            --no-summary "$INPUT" 2>&1

# Exit with the clamdscan exit code (0=>OK 1=>Virus signature 2=>Error)
exit "$?"

