#!/bin/sh
# Usage: $0 <Directory to watch for> [...<Directory to watch for>]
# @see https://www.freedesktop.org/software/systemd/man/sd-daemon.html (logging)
# @see https://www.freedesktop.org/software/systemd/man/systemd.exec.html (exit codes)
# Distributed by clamscan-inotify

# Clamdscan-verify isn't executable : exit (critical OS file missing in sysexits.h)
[ ! -x "$(command -v clamdscan-verify.sh)" ] && \
    { echo >&2 "<3>Clamdscan-verify not found or not an executable, exit" && exit 72; }

DIRECTORY_LIST=
for DIRECTORY in "$@"
do

    # Called without directory : log warning
    [ -z "$DIRECTORY" ]   && \
        { echo >&2 "<4>No directory specified" && continue; }

    # Argument passed isn't a directory : log warning
    [ ! -d "$DIRECTORY" ] && \
        { echo >&2 "<4>Argument is not a directory: $DIRECTORY" && continue; }

    # Directory is not readable : log warning
    [ ! -r "$DIRECTORY" ] && \
        { echo >&2 "<4>Directory is not readable: $DIRECTORY" && continue; }

    # Directory is not writable : log notice
    [ ! -w "$DIRECTORY" ] && echo >&2 "<5>Directory added, but not writable: $DIRECTORY"

    # Add directory to list
    DIRECTORY_LIST="$DIRECTORY_LIST $DIRECTORY"

done

# Cleanup first space in directory list
DIRECTORY_LIST="$(echo "$DIRECTORY_LIST" | sed 's|^\ ||')"

# Where communicate to XORG bus
export DISPLAY
export DBUS_SESSION_BUS_ADDRESS

echo "<5>$0 started for $(whoami) looking for changes in: $*"

# Scan for write changes into given directories
inotifywait --quiet --monitor --recursive \
    --event moved_to,close_write          \
    --format "%w%f" "$DIRECTORY_LIST"   | \
while read -r FILEPATH
do

    # Change is an empty filepath
    [ ${#FILEPATH} -eq 0 ] && continue

    # Directories itselfs are not verified there (directories contents are)
    [ -d "${#FILEPATH}" ] && continue

    # Browsers move or rename ".part" files just after the download's end
    sleep 1s && [ ! -f "$FILEPATH" ] && continue

    # Verify file
    OUTPUT=$("$(command -v clamdscan-verify.sh)" "$FILEPATH")

    # Notify user and write to journal on virus signature ($?=1) or error ($?=2)
    [ $? -ge 1 ]                                                            && \
        RESULT="$(echo "$OUTPUT" | head -n1 )"                              && \
        echo "$RESULT" | "$(command -v clamdscan-notify.sh)" "$FILEPATH"    && \
        printf "<3>%-s for %-s\\n" "$RESULT" "$(whoami)" >&2 &

done

