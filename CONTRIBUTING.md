You are welcome to propose improvements and submit issues!

Fork the repository, work with git-flow from the develop branch,
on a new feature or fix, and propose a merge of your branch.

Public source code is at 
https://codeberg.org/sballeste/clamscan-inotify

A debian package build script is provided in */build-deb.sh*.
It works on the server side with a mini-dinstall and dput.
You need asciidoctor-pdf to generate the manual page.

General workflow is :

- git checkout next
- git flow start (feature|hotfix|release) {feature-name} # creates the feature
- git add ... git commit # next
- ./build-deb.sh "new stuff" # package chain

