#!/bin/bash
# @see https://www.debian.org/doc/packaging-manuals/debconf_specification.html
# @see https://manpages.ubuntu.com/manpages/precise/fr/man7/debconf-devel.7.html
# This script can be called with parameter :
# - $1="configure" while apt installation or upgrade
# - $1="reconfigure" and $2="package version" while dpkg-reconfigure operation

# Trust configuration errors, don't break installation
# export DEBCONF_DEBUG=developer
set +e

# source debconf shell functions
# You pass parameters to them and get a result back in the $RET variable.
. '/usr/share/debconf/confmodule'

# Hability to go back
db_capb backup

# Clamscan-inotify configuration
CONFIGFILE=/etc/clamav/clamscan-inotify-server.conf

# systemd initializations after configuration : enable/start user services
POSTINST_SCRIPT="/tmp/clamscan-inotify-postinst.sh"
[ -f "$POSTINST_SCRIPT" ] && rm -f "$POSTINST_SCRIPT"
touch "$POSTINST_SCRIPT"

# Fetch previous values, transform config file to sourcable file
if [ -e "$CONFIGFILE" ]; then

    TMP_CONFIGFILE="/tmp/clamscan-inotify-server-temp.conf"

    sed 's/\ /=/;s/^#.*//' "$CONFIGFILE"  | \
        grep -v '^$' > "$TMP_CONFIGFILE" && \
        . "$TMP_CONFIGFILE"

    rm -f "$TMP_CONFIGFILE"

    # Save read properties in debconf
    db_set  clamscan-inotify/clamd_IP        "$TCPAddr"
    db_set  clamscan-inotify/clamd_PORT      "$TCPSocket"

fi

# Create default clamdscan config
CLAMDSCAN_CONFIG="/etc/clamav/clamd.conf"
[ -e "$CONFIGFILE" ] && [ ! -e "$CLAMDSCAN_CONFIG" ] && \
    ln -vs "$CONFIGFILE" "$CLAMDSCAN_CONFIG"

# Cleanup list for multiselect : sort uniques,
# remove "," at start and list uniques entries and the
# ExecStart=/usr/bin/clamdscan-inotify.sh line itself if present
function dh_select_clean_list
{

    # At least one parameter is required
    [ -z "$1" ] && return

    echo "$@" | tr ',' '\n' | sed -r 's/^\s*(\S+(\s+\S+)*)\s*$/\1/' | \
    sort -u | sed '/^$/d' | \
    tr '\n' ',' | sed 's/^,//;s/,$//;s/,/,\ /g' | \
    sed -r 's|ExecStart=/usr/bin/clamdscan-inotify.sh||g'

}

# Q1 : Get clamav-daemon IP
db_input high clamscan-inotify/clamd_IP
db_go
db_get clamscan-inotify/clamd_IP || true
clamd_IP="$RET"

# Q2: (if question 1 has been answered)
db_input high clamscan-inotify/clamd_PORT
db_go
db_get clamscan-inotify/clamd_PORT || true
clamd_PORT="$RET"

# Configure clamscan client
sed -ri 's/^TCPSocket\ (.*)/TCPSocket\ '$clamd_PORT'/;s/^TCPAddr\ (.*)/TCPAddr\ '$clamd_IP'/' \
        "$CONFIGFILE"

# Enabling service for users :
UID_MIN=$(grep "^UID_MIN" '/etc/login.defs' | grep -oE '[^ ]+$')
UID_MAX=$(grep -v nobody /etc/passwd | cut -d : -f 3 | sort -n | tail -n 1)

# Force systemd daemon reload
echo 'systemctl daemon-reload' >> "$POSTINST_SCRIPT"

# Foreach UID in the system
for ((USER_ID=UID_MIN;USER_ID<=UID_MAX;USER_ID++));
do

    # HOME directory
    USER_HOME="$(getent passwd $USER_ID | cut -d : -f 6)"

    # User don't have home
    [ -z "$USER_HOME" ] && continue

    # User NAME
    USER_NAME="$(getent passwd $USER_ID | cut -d : -f 1)"

    # Systemd template
    USER_UNIT_TPL="/lib/systemd/system/clamscan-inotify@.service"

    # systemd user unit file
    USER_UNIT_FILE="/lib/systemd/system/clamscan-inotify@$USER_NAME.service"

    # The symbolic link to target, later build
    USER_UNIT_FILE_INSTALLED="/etc/systemd/system/multi-user.target.wants/clamscan-inotify@$USER_NAME.service"

    # Q3 : Change actual service state
    db_title "clamscan-inotify for $USER_NAME"

    # Actual state of service : disabled if unit not found
    state_now="$(systemctl is-enabled clamscan-inotify@$USER_NAME.service 2>/dev/null)"
    [ "$state_now" = '' ] && state_now='disabled'

    # Default action is to do nothing
    [ "$state_now" = 'disabled' ] && \
        { ENABLE_SERVICE=false && state_after='enabled'; } || \
        { ENABLE_SERVICE=true && state_after='disabled'; }
    db_set clamscan-inotify/service_enable $ENABLE_SERVICE

    # String substitutions
    db_subst clamscan-inotify/service_enable USER_NAME "$USER_NAME"
    db_subst clamscan-inotify/service_enable state_now "$state_now"
    db_subst clamscan-inotify/service_enable state_after "$state_after"

    # Ask question
    db_input low clamscan-inotify/service_enable
    db_go || true

    # Get result
    db_get clamscan-inotify/service_enable || true

    # Disable/Enable service ?
    ENABLE_SERVICE="$RET"
    if [ "$ENABLE_SERVICE" = true ]; then

        # Enabled the service if not already enabled
        [ "$state_now" != 'enabled' ] && \
            dh_installsystemd --restart-after-upgrade "clamscan-inotify@$USER_NAME.service" && \
            deb-systemd-helper enable "clamscan-inotify@$USER_NAME.service"

        # Be sure the unit is installed from template
        [ ! -f "$USER_UNIT_FILE" ] && \
            cp -a "$USER_UNIT_TPL" "$USER_UNIT_FILE"

        # Be sure the service is enabled, with a symlink to the unit
        [ ! -L "$USER_UNIT_FILE_INSTALLED" ] && \
            ln -s   "$USER_UNIT_FILE" "$USER_UNIT_FILE_INSTALLED"

    else

        # Disabled the service if not already disabled
        [ "$state_now" != 'disabled' ] && \
            deb-systemd-helper disable "clamscan-inotify@$USER_NAME.service"

        # Be sure the service is disabled, the link to unit has been removed
        [ -L "$USER_UNIT_FILE_INSTALLED" ] && rm -f "$USER_UNIT_FILE_INSTALLED"

        # Stop configuration there
        continue

    fi

    # Reload units
    systemctl daemon-reload

    # Q4: Select Directories to scan in realtime
    db_title "clamscan-inotify directories for $USER_NAME"
    db_subst clamscan-inotify/service_directories USER_NAME "$USER_NAME"

    # List of user XDG directories for dh_subst
    DIRECTORIES=
    for DIRECTORY in DESKTOP DOWNLOAD TEMPLATES PUBLICSHARE DOCUMENTS MUSIC PICTURES VIDEOS
    do

        # Get XDG directory PATH
        XDG_DIR=$(sudo -u "$USER_NAME" xdg-user-dir "$DIRECTORY" | sed 's|\ |\\\ |g')
        # Add directory to list, if possible
        [ $(sudo -u "$USER_NAME" /bin/sh -c "test -d $XDG_DIR && test -w $XDG_DIR && echo ok") = "ok" ] && \
            DIRECTORIES+=", $XDG_DIR"

    done

    # The user set previously it's directories
    SELECTED_DIRECTORIES=
    [ -e "$USER_UNIT_FILE_INSTALLED" ] &&
        SELECTED_DIRECTORIES+=$(grep '^ExecStart=/usr/bin/clamdscan-inotify' \
            "$USER_UNIT_FILE_INSTALLED" | sed -r 's|^ExecStart=/usr/bin/clamdscan-inotify\.sh\ (.*)|\1|' | \
            sed 's|\ |,\ |g')
    [ ${#SELECTED_DIRECTORIES} -gt 0 ] && \
        DIRECTORIES+=", $SELECTED_DIRECTORIES"

    SELECTED_DIRECTORIES="$(dh_select_clean_list "$SELECTED_DIRECTORIES")"
    DIRECTORIES="$(dh_select_clean_list "$DIRECTORIES")"

    # String substitutions
    db_subst clamscan-inotify/service_directories DIRECTORIES "$DIRECTORIES"
    db_subst clamscan-inotify/service_directories SELECTED_DIRECTORIES "$SELECTED_DIRECTORIES"

    # Ask question and get result
    db_input low clamscan-inotify/service_directories
    db_go || true
    db_get clamscan-inotify/service_directories || true

    # Replace directories to verify in user's service configuration
    DIRS="$(echo $RET | tr ',' ' ' | sed 's|\ \ |\ |g;s|\ |\\\ |g')"
    [ "$1" = "reconfigure" ] && \
        sed -ri "s|^(ExecStart=/usr/bin/clamdscan-inotify\.sh).*|\1 $DIRS|" "$USER_UNIT_FILE"

    # Replace user ID in user's service configuration
    sed -ri "s/USER_ID/$USER_ID/" "$USER_UNIT_FILE"

    # Stop service
    [ $(systemctl is-active "clamscan-inotify@$USER_NAME.service") = 'active' ] && \
        echo "systemctl stop clamscan-inotify@$USER_NAME.service" >> "$POSTINST_SCRIPT"

    # Reload units
    systemctl daemon-reload

    # Start service is enabled previously for the user
    if [ "$ENABLE_SERVICE" = true ]; then

        systemctl start clamscan-inotify@$USER_NAME.service
        echo "systemctl start clamscan-inotify@$USER_NAME.service" >> "$POSTINST_SCRIPT"

    fi

    db_go || true

    # File-manager script integration for nautilus, caja, nemo and thunar.
    # Only tested with nautilus
    SCRIPT_NAME='clamdscan'
    SCRIPT_NAME_PREV='clamdscan🛡'
    test -f '/usr/share/fonts/truetype/noto/NotoColorEmoji.ttf' && \
        SCRIPT_NAME='clamdscan🛡' && SCRIPT_NAME_PREV='clamdscan'

    for SCRIPT_PATH in '.local/share/nautilus/scripts' \
                       '.config/caja/scripts'          \
                       '.local/share/nemo/scripts'     \
                       '.config/thunar/scripts'
    do

        if [ ! -L "$USER_HOME/$SCRIPT_PATH/$SCRIPT_NAME" ]; then

            # Create nautilus script home
            [ ! -d "$USER_HOME/$SCRIPT_PATH" ] && \
                sudo -u "$USER_NAME" /bin/sh -c "mkdir -p $USER_HOME/$SCRIPT_PATH"

            # Symlink to provide a right-click to verify/notify script
            sudo -u "$USER_NAME" /bin/sh -c "ln -sv /usr/share/nautilus-script/clamdscan $USER_HOME/$SCRIPT_PATH/$SCRIPT_NAME"

        fi

        # remove old script symlink
        [ -L "$USER_HOME/$SCRIPT_PATH/$SCRIPT_NAME_PREV" ] && \
            rm "$USER_HOME/$SCRIPT_PATH/$SCRIPT_NAME_PREV"

    done

done

db_go || true

# Stop more configuration
db_stop

# Run needrestart
echo '[ -x "$(command -v needrestart)" ] && needrestart -r a' >> "$POSTINST_SCRIPT"



