#!/bin/bash
# Create a debian package structure
# @author seb <seb@fitou.lan>
# @install sudo apt install dh-make

dh_make --packagename                                       \
    $(basename "$PWD" | sed 's/\.git/_'$(date '+%y')'/')    \
    --native                                                \
    --email "$EMAIL"                                        \
    --indep                                                 \
    --copyright custom                                      \
    --copyrightfile COPYING && touch "$PWD/debian/COPYING"

